
import './App.css';
import { SignForms } from './components/SignForms';



function App() {
  return (
    <div className="App">
     <SignForms />
    </div>
  );
}

export default App;
